#Dependencies
require "csv"
require "sunlight"


class EventManager
	INVALID_PHONE_NUMBER = "0000000000"
	INVALID_ZIP_CODE = "00000"
	CHARACTERS_TO_REMOVE = /\D/
	Sunlight::Base.api_key = "e179a6973728c4dd3fb1204283aaccb5"

	def initialize(filename)
		puts "EventManager Initialized."
		@file = CSV.open(filename, {headers: true, header_converters: :symbol} )
	end

	def print_names
		@file.each do |line|
			puts line.inspect
			#puts line[:first_name] +" " + line[:last_name] 
		end
	end

	def print_numbers
		@file.each do |line|
			number = clean_number(line[:homephone])
			puts number
		end
	end

	def print_zipcodes
		@file.each do |line|
			zip =  clean_zipcode(line[:zipcode])
			puts zip
		end
	end

	def output_data(clean_file_name)
		output = CSV.open(clean_file_name, "w")
		@file.each do |line|
			if @file.lineno == 2
				output << line.headers 
			end
			line[:homephone] = clean_number(line[:homephone])
			line[:zipcode] = clean_zipcode(line[:zipcode])
			line[:first_name] = clean_name(line[:first_name])
			line[:last_name] = clean_name(line[:last_name])
			output << line
		end
	end

	def rep_lookup
		20.times do
			line = @file.readline
			legislators = Sunlight::Legislator.all_in_zipcode(clean_zipcode(line[:zipcode]))
			names = legislators.collect do |leg|
				first_name = leg.firstname
				first_initial = first_name[0]
				last_name = leg.lastname
				party = leg.party
				title = leg.title

				"#{title}. " + first_initial + "." + last_name + " (#{party})"

			end
			puts "#{line[:last_name]}, #{line[:first_name]}, #{line[:zipcode]}, #{names.join(", ")}"
		end
	end

	def create_form_letter
		letter =  File.open("form_letter.html", "r").read
		20.times do
			line = @file.readline
			custom_letter = letter.gsub("\#first_name", line[:first_name].to_s.capitalize)
			custom_letter = custom_letter.gsub("#last_name", line[:last_name].to_s)
			custom_letter = custom_letter.gsub("#street", line[:street].to_s)
			custom_letter = custom_letter.gsub("#city", line[:city].to_s)
			custom_letter = custom_letter.gsub("#state", line[:state].to_s)
			custom_letter = custom_letter.gsub("#zipcode", clean_zipcode(line[:zipcode]).to_s)
			filename =  "output/thanks_#{line[:first_name]}_#{line[:last_name]}.html"
			output = File.new(filename, "w")
			output.write(custom_letter)
		end
	end

	def rank_times
		hours = Array.new(24) {0}
		@file.each do |line|
			hour = line[:regdate].split(" ")[1].split(":")[0].to_i
			hours[hour] += 1
		end
		hours.each_with_index{|counter, hour| puts "#{hour}\t#{counter}"}
	end

	def day_stats
		days = Array.new(7) {0}
		@file.each do	|line|
			date = line[:regdate].split(" ")[0] # ex: 11/12/08
			day = Date.strptime(date, "%m/%d/%y") # 11 is the month, 25 is the day, 08 is the year
			days[day.wday] += 1
		end
		days.each_with_index{|num, day| puts "#{day}\t#{num}"}
	end

	def state_stats
		state_data = {}
		@file.each do	|line|
			state = line[:state]
			if state_data[state].nil?
				state_data[state] = 1
			else
				state_data[state] += 1
			end
		end
		# state_data = state_data.select{ |state, count| state }.sort_by{ |key, value| -value }
		# state_data.each { |state, count| puts "#{state}: #{count}"}
		ranks = state_data.sort_by{|state, counter| -counter}.collect{|state, counter| state}
		state_data = state_data.select{|state, counter| state}.sort_by{|state, counter| state}
		state_data.each do |state, counter|
  		puts "#{state}:\t#{counter}\t(#{ranks.index(state) + 1})"
		end
	end

private

	def clean_number(number)
		number = number.gsub(CHARACTERS_TO_REMOVE, "")
		number = verify_length(number)
		return number
	end

	def verify_length(number)
		case number.length
		when 10
			number
		when 11 && number.start_with?("1")
			number = number[1..-1] 
		else
			number = INVALID_PHONE_NUMBER
		end
		#always returns a string of 10 numbers	
	end

	def clean_zipcode(zip)
		zip = INVALID_ZIP_CODE if zip == nil
		zip = "0" + zip until zip.length == 5
		return zip
	end

	def clean_name(name)
		name = name.capitalize
		name
	end


end

manager =  EventManager.new("event_attendees.csv")
#manager.output_data "event_attendees_clean.csv"
manager.state_stats
